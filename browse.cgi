#!/usr/bin/perl -wT
print "Content-type: text/html; charset=iso-8859-1\n\n";

use CGI;
$cgi = new CGI;

$menu = 'browse';

$folder = $cgi->param('folder');

require './header.pl';
 
print '    <div id="content" role="main">';
    
print '        <article class="post">';
print '            <h2 class="entry-title"><a href="#">'.ucfirst($folder).'</a></h2>';
if( -d "$folder/" ) {
   	foreach my $file ( <$folder/*.*> ) {
   		my $taille = -s $file;
   		if ($taille > 1048576) {
   			$taille = $taille / 1048576;
   			$taille = sprintf("%.2f", $taille) . ' Mo';	
	   	} elsif ($taille > 1024) {
   			$taille = $taille / 1024;
   			$taille = sprintf("%.2f", $taille) . ' Ko';	
   		} else {
   			$taille = sprintf("%.2f", $taille) . ' o';
   		}
   		
		if( -f $file ) {
			print '            <footer class="post-meta">';
	   		print '					<p>';
			print split(/$folder\//,$file);
			print '<span style="float:right;">';
			print $taille;
			print '</span>';
			print '					</p>';
			print '                	<a href="'.$file.'" class="more-link">Download</a>';
			print '            </footer>';
		}
	}
} else {
	print "Cannot find $folder folder.\n"
}

print '        </article';
        
print '        <hr />';
print '    </div>';
    
print '    <aside id="sidebar" role="complementary">';
    
print '        <aside class="widget">';
print '            <h3>Folders</h3>';
            
print '            <ul>';
print '                <li><a href="browse.cgi?folder=movies">Movies</a></li>';
print '                <li><a href="browse.cgi?folder=pictures">Pictures</a></li>';
print '                <li><a href="browse.cgi?folder=musics">Musics</a></li>';
print '                <li><a href="browse.cgi?folder=documents">Documents</a></li>';
print '            </ul>';
print '        </aside>';
print '    </aside>';

require './footer.pl';