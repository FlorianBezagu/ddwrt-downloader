#!/bin/bash
 
function downMU {

if [ [-z "$1"] && [-z "$2"] ]
then
        echo "need an url & folder as parameters"
else
        echo true > ./system/muInProgress.txt

        /usr/local/bin/wget -O /temp/index.html $2
        url=`cat /temp/index.html* |grep "files"|sed -e 's/.*"\(http[^"]*\)".*/\1/g'`
        filename=${url##*/}
        id=${2##*\=}
        rm /temp/index.html*
        sleep 45
        /usr/local/bin/wget -O ./$1/$filename $url -o ./inProgress/mu/$id.txt
        rm ./inProgress/mu/$id.txt
        echo false > ./system/muInProgress.txt
        # wget $url
fi
}

isEnabled=`cat ./system/muEnabled.txt`

if [[ $isEnabled == "false" ]]
then
echo true > ./system/muEnabled.txt
echo "false\n"
for line in `cat downUrl|grep -v "^#"`
do
        count=`cat downUrl |wc -l`
        # echo "Downloading : $line"
        folder=`echo $line | awk -F";" '{print $1}'`
        link=`echo $line | awk -F";" '{print $2}'`
        downMU $folder $link
 
        #on commente l'url courante (au cas ou le dl
        #a planté on aura juste a dc
        # echo "#`head -1 downUrl`">>downUrl
        # tail -$count downUrl > test
        # mv test downUrl
        sleep 1 #just to wait a bit
done
echo false > ./system/muEnabled.txt
else
        echo "true"
fi