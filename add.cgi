#!/usr/bin/perl -wT
print "Content-type: text/html; charset=iso-8859-1\n\n";

$menu = 'add';

require './header.pl';
 
print '    <div id="content" role="main">';
    
print '        <article class="post">';
# print '            <h2 class="entry-title"><a href="#">Add a download</a></h2>';
print '			   <form action="download.pl" method="post">';
print '			   		<label class="inline" for="type">Select your download\'s type : </label>';
print '			   		<select class="inline" name="type">';
print '						<option value="direct">Direct Link</option>';
print '						<option value="mu">MegaUpload Link</option>';
print '			   		</select>';
print '    				<div class="clear"></div>';
print '			   		<label class="inline" for="folder">Select your folder : </label>';
print '			   		<select class="inline" name="folder">';
print '						<option value="movies">Movies</option>';
print '						<option value="pictures">Pictures</option>';
print '						<option value="musics">Musics</option>';
print '						<option value="documents">Documents</option>';
print '			   		</select>';
print '    				<div class="clear"></div>';
print '            		<footer class="post-meta">';
print '                		<input type="text" name="link"/>';
print '						<input style="margin-left:20px;" type="submit" value="Download"/>';
print '            		</footer>';
print '			   </form>';
print '        </article';
        
print '        <hr />';
print '    </div>';

require './footer.pl';