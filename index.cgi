#!/usr/bin/perl -wT
print "Content-type: text/html; charset=iso-8859-1\n\n";

$menu = 'download';

use CGI;
$cgi = new CGI;

require './header.pl';

# Gestion des direct download
print '    <div id="content" role="main" style="margin-bottom:30px;">';    
print '        <article class="post">';
print '            <h2 class="entry-title"><a href="#">Direct</a></h2>';
$folder = "inProgress/direct/";
if( -d "$folder" ) {
	foreach my $file ( <$folder/*.*> ) {
		# Test si le repertoire existe
		if( -f $file ) {
			# Recuperation de l'avant derniere ligne
			open(FICHIER,"<$file");
			my @lignes = (reverse(<FICHIER>))[1];
			close(FICHIER);
			print '            <footer class="post-meta">';
			print '					<p>';
			# Affiche le nom du fichier
			print split(/$folder\//,$file);
			print '<span style="float:right;">';
			# Enleve les '.'
			@percent = split(/.......... /,"@lignes", 6);
			# Enleve les espaces
			@percent = split(/\s+/,"@percent",5);
			# Affiche le pourcentage et le temps restant
			print @percent[2].' - '.@percent[4].' remaining';
			print '</span>';
			print '					</p>';
			print '            </footer>';
		}
	}
} else {
	print "Cannot find folder.\n"
}
print '        </article';
print '        <hr />';
print '    </div>';

# Gestion des megaupload
print '    <div id="content" role="main">';
print '        <article class="post">';
print '            <h2 class="entry-title"><a href="#">MegaUpload</a></h2>';
$folder = "inProgress/direct/";
if( -d "$folder" ) {
	foreach my $file ( <$folder/*.*> ) {
		# Test si le repertoire existe
		if( -f $file ) {
			# Recupere la derniere ligne
			open(FICHIER,"<$file");
			my @ligne = (<FICHIER>)[0];
			close(FICHIER);
			# Split du ';'
			@ligne = split(/;/,"@ligne");
			# Si waiting alors on affiche les 45 secondes
			if (@ligne[0] eq 'waiting') {
				print '            <footer class="post-meta">';
				print '					<p>';
				# Affiche le nom du fichier
				print split(/$folder\//,$file);
				print '<span style="float:right;">';
				# Affiche le message des 45 secondes
				print 'Waiting 45 sec...';
				print '</span>';
				print '					</p>';
				print '            </footer>';
			} else {
				# Sinon telechargement en cours
				# Recuperation de l'avant derniere ligne
				open(FICHIER,"<$file");
				my @lignes = (reverse(<FICHIER>))[1];
				close(FICHIER);
				print '            <footer class="post-meta">';
				print '					<p>';
				# Affiche le nom du fichier
				print split(/$folder\//,$file);
				print '<span style="float:right;">';
				# Enleve les '.'
				@percent = split(/.......... /,"@lignes", 6);
				# Enleve les espaces
				@percent = split(/\s+/,"@percent",5);
				# Affiche le pourcentage et le temps restant
				print @percent[2].' - '.@percent[4].' remaining';
				print '</span>';
				print '					</p>';
				print '            </footer>';
			}
		}
	}
} else {
	print "Cannot find folder.\n"
}
print '        </article';        
print '        <hr />';
print '    </div>';

require './footer.pl';