print '<!doctype html>';
print '<head>';
print '    <meta charset="utf-8" />';
print '    <title></title>';

print '    <meta name="description" content="" />';
    
print '    <link href="/ddwrt-downloader-static/css/style.css" rel="stylesheet" />';
print '    <link href="http://fonts.googleapis.com/css?family=Droid+Serif:regular,bold" rel="stylesheet" /> <!-- Load Droid Serif from Google Fonts -->';

print '</head>';

print '<body>';

print '<div id="wrapper">';

print '    <header id="header" class="clearfix" role="banner">';
    
print '        <hgroup>';
print '            <h1 id="site-title"><a href="index.cgi">OpenWRT Downloader</a></h1>';
print '            <h2 id="site-description">Awesome!</h2>';
print '        </hgroup>';
    
print '    </header>';

print '<div id="main" class="clearfix">';

print '    <nav id="menu" class="clearfix" role="navigation">';
print '        <ul> ';
print '            <li '; 
print 'class="current"' if $menu eq 'download';
print '><a href="index.cgi">Downloads in progress</a></li>';
print '            <li ';
print 'class="current"' if $menu eq 'add';
print '><a href="add.cgi">Add a download</a></li>';
print '            <li ';
print 'class="current"' if $menu eq 'browse';
print '><a href="browse.cgi?folder=movies">Browse</a></li>';
print '        </ul>';
print '    </nav>';