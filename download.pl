#!/usr/bin/perl -w
use CGI;
use URI::Escape;

# recupere  l'entree standard dans la variable $in
read(STDIN, $in, $ENV{CONTENT_LENGTH});

# la chaine $in est coupee suivant le caractere & et cree la liste @champs
@champs = split(/&/,$in);

# traitement de chaque element $e de la liste @champs
foreach $e (@champs) {
  # dissocie chaque element, de la forme nom=valeur, 
  # en une paire de variable (nom,valeur)
  ($nom, $valeur) = split(/=/,$e);
	
  # transforme tous les caracteres saisis en minuscules
  $valeur =~  tr/A-Z/a-z/;
	
  # cree a partir du tableau @champs, 
  # une liste associative %champs
  $champs{$nom}=$valeur;
}


#print("Content-Type: text/html\n\n");

while (($nom, $valeur) = each(%champs)) {
	if ($nom eq "type") {
		$type = $valeur;
	} elsif ($nom eq "link") {
		$link = uri_unescape($valeur);
	} elsif ($nom eq "folder") {
    $folder = $valeur;
  }
}

if($type eq "mu") {
  @args = ("/bin/sh", "./download-mu.sh", $folder, $link);
} elsif($type eq "direct") {
  @args = ("/bin/sh", "./download-direct.sh", $folder, $link, '&');

  $system = "/bin/sh ./download-direct.sh $folder $link";

  system $system;

}

# echo "movies;http://www.megaupload.com/?d=61X8RE55" >> downUrl

# @args = ("/bin/sh", "./download-http.sh");
# system(@args);


my $url = "http://localhost/cgi-bin/ddwrt-downloader/index.cgi";
print "Location: $url\n\n";